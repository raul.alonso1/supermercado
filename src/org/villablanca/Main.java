package org.villablanca;
import java.util.*;
public class Main {
	
	private static int[] cantidades;
	private static String[] productos;
	private static int contador;

	public static void eliminarProducto(int[] cantidades, String[] productos, int numeroProductos, int posicion) {
		productos[posicion] = productos[numeroProductos-1];
		cantidades[posicion] = cantidades[numeroProductos-1];
		productos[numeroProductos-1] = null;
		cantidades[numeroProductos-1] = 0;	
	}
	
	public static int mostrarMenu() {
		Scanner teclado= new Scanner(System.in);
		int opcion;
		do {
			System.out.println("1.Añadir producto");
			System.out.println("2.Eliminar producto");
			System.out.println("3.Calcular media producto");
			System.out.println("4.Salir del menu");
			opcion=teclado.nextInt();
		}while(opcion<1&&opcion>4);
	
		return opcion;
	}
	
	
	public static void main(String[] args) {
		// TODO Esbozo de método generado automáticamente

	}

}
